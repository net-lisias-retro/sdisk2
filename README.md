# SDisk2
SD card interface for Apple 2 computers

# Authors
* (c) 2010 Koichi NISHIDA <tulip-house@msf.biglobe.ne.jp>
* (c) 2012 Victor Trucco
* (c) 2012 Fábio Belavenuto
* (c) 2015 Alexandre Suaide
* (c) 2017 Lisias Toledo (unix tools)

# License
This work is released under the version 3 and/or 2 of the GNU General Public License. See below for details.

'''NOTE:'''
The current legal status of this code is uncertain.

Fábio and Alexandre have published files with a header that references the version 3 of the GNU General Public License, but it is not clear yet what's the licensing chosen by the original author, Koichi Nishida.

This repository will be updated once the actual licensing info is clarified by the authors.

The UNIX Tools is (C) Lisias Toledo, and are licensed **under GPL 2 or later**, at licensee's discretion.

# Sources
forked from: https://github.com/suaide/SDisk2

Nishida's page: http://tulip-house.ddo.jp/DIGITAL/SDISK2/english.html