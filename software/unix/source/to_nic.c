/*
 * to_nic
 *
 * UNIX Filter to convert the .dsk format to the NIC suitable for the SDisk2
 *
 * (c) 2017 Lisias Toledo. Licensed under the GPL 2 or later.
 */

#include <stdio.h>
#include <string.h>

#define ARRAYSIZE(v) (sizeof(v)/sizeof(v[0]))

static unsigned char const encTable[] = {
        0x96,0x97,0x9A,0x9B,0x9D,0x9E,0x9F,
        0xA6,0xA7,0xAB,0xAC,0xAD,0xAE,0xAF,
        0xB2,0xB3,0xB4,0xB5,0xB6,0xB7,0xB9,0xBA,0xBB,0xBC,0xBD,0xBE,0xBF,
        0xCB,0xCD,0xCE,0xCF,
        0xD3,0xD6,0xD7,0xD9,0xDA,0xDB,0xDC,0xDD,0xDE,0xDF,
        0xE5,0xE6,0xE7,0xE9,0xEA,0xEB,0xEC,0xED,0xEE,0xEF,
        0xF2,0xF3,0xF4,0xF5,0xF6,0xF7,0xF9,0xFA,0xFB,0xFC,0xFD,0xFE,0xFF
	};

static unsigned char const SKEW_PHYSICAL_TO_DOS[] = {
	0, 7, 14, 6, 13, 5, 12, 4, 11, 3, 10, 2, 9, 1, 8, 15
	};
static unsigned char const SKEW_PHYSICAL_TO_PRODOS[] = { // Pascal & FORTRAN too
	0, 8, 1, 9, 2, 10, 3, 11, 4, 12, 5, 13, 6, 14, 7, 15
	};
static unsigned char const SKEW_PHYSICAL_TO_CPM[] = {
	0, 3, 6, 9, 12, 15, 2, 5, 8, 11, 14, 1, 4, 7, 10, 13
	};

static unsigned char const FlipBit1[] = { 0, 2,  1,  3  };
static unsigned char const FlipBit2[] = { 0, 8,  4,  12 };
static unsigned char const FlipBit3[] = { 0, 32, 16, 48 };

#define writeVal(val, buf, ndx) ( buf[ndx++] = val )
#define writeBuf(buf, buf_sz, src_buf, src_ndx) { memcpy(src_buf + src_ndx, buf, buf_sz); src_ndx += buf_sz; }

static void writeAAVal(unsigned char const val, unsigned char * const buf, size_t * const ndx)
{
	buf[(*ndx)++] = (0xaa | (val >> 1));
	buf[(*ndx)++] = (0xaa | val);
}

int conv_to_nic(
	int const volume,
	unsigned char const * const skew_table, int const num_sectors_per_track,
	FILE * const fin, FILE * const fout
)
{
	for (int track = 0; ; track++) {
		unsigned char track_buf[256*num_sectors_per_track];
		size_t iobytes;

		if ( sizeof(track_buf) != (iobytes = fread(track_buf, sizeof(track_buf[0]), ARRAYSIZE(track_buf), fin)) )
		{
			// Acabou
			if (0 != iobytes)
			{
				fprintf(stderr, "deu merda!\n");
				return -1;
			}
			return 0;
		}

		for (int physical_sector_num = 0; physical_sector_num < num_sectors_per_track; physical_sector_num++) {
			size_t rawsector_buf_ndx = 0;
			unsigned char rawsector_buf[512]; // Padding a sector to 512, so a track will use 8192 bytes - padding to a FAT32 cluster.

			int const dos_sector_num = skew_table[physical_sector_num];

			// Write sync bytes
			for (int i=0; i < 22; i++) writeVal(0xff, rawsector_buf, rawsector_buf_ndx);
			writeVal(0x03, rawsector_buf, rawsector_buf_ndx);
			writeVal(0xfc, rawsector_buf, rawsector_buf_ndx);
			writeVal(0xff, rawsector_buf, rawsector_buf_ndx);
			writeVal(0x3f, rawsector_buf, rawsector_buf_ndx);
			writeVal(0xcf, rawsector_buf, rawsector_buf_ndx);
			writeVal(0xf3, rawsector_buf, rawsector_buf_ndx);
			writeVal(0xfc, rawsector_buf, rawsector_buf_ndx);
			writeVal(0xff, rawsector_buf, rawsector_buf_ndx);
			writeVal(0x3f, rawsector_buf, rawsector_buf_ndx);
			writeVal(0xcf, rawsector_buf, rawsector_buf_ndx);
			writeVal(0xf3, rawsector_buf, rawsector_buf_ndx);
			writeVal(0xfc, rawsector_buf, rawsector_buf_ndx);

			// Address Field mark
			writeVal(0xd5, rawsector_buf, rawsector_buf_ndx);
			writeVal(0xaa, rawsector_buf, rawsector_buf_ndx);
			writeVal(0x96, rawsector_buf, rawsector_buf_ndx);

			// Address block
			writeAAVal(volume, rawsector_buf, &rawsector_buf_ndx);
			writeAAVal(track, rawsector_buf, &rawsector_buf_ndx);
			writeAAVal(physical_sector_num, rawsector_buf, &rawsector_buf_ndx);
			writeAAVal(volume ^ track ^ physical_sector_num, rawsector_buf, &rawsector_buf_ndx);

			// End of Field mark
			writeVal(0xde, rawsector_buf, rawsector_buf_ndx);
			writeVal(0xaa, rawsector_buf, rawsector_buf_ndx);
			writeVal(0xeb, rawsector_buf, rawsector_buf_ndx);

			// Some spacing sync bytes
			for (int i=0; i<5; i++) writeVal(0xff, rawsector_buf, rawsector_buf_ndx);

			// Data Field mark
			writeVal(0xd5, rawsector_buf, rawsector_buf_ndx);
			writeVal(0xaa, rawsector_buf, rawsector_buf_ndx);
			writeVal(0xad, rawsector_buf, rawsector_buf_ndx);

			{
				unsigned char x, ox = 0;
				unsigned char rawsector[342];
				size_t const indx = 256*dos_sector_num;
				unsigned char const * const s = track_buf + indx;

				for (int i = 0; i < 84; i++) {
					x = FlipBit1[s[i]&3] | FlipBit2[s[i+86]&3] | FlipBit3[s[i+172]&3];
					rawsector[i]=encTable[(x^ox)&0x3f];
					ox = x;
				}
				for (int i = 84; i < 86; i++) { // Not really needed, but I made this extra loop to make NICs equal to the original SDISK firmware.
					x = FlipBit1[s[i]&3] | FlipBit2[s[i+86]&3];
					rawsector[i]=encTable[(x^ox)&0x3f];
					ox = x;
				}
				for (int i = 0; i < 256; i++) {
					x = (s[i] >> 2);
					rawsector[i+86] = encTable[(x^ox)&0x3f];
					ox = x;
				}

				writeBuf(rawsector, sizeof(rawsector), rawsector_buf, rawsector_buf_ndx);

				// CHKSUM
				writeVal(encTable[ox&0x3f], rawsector_buf, rawsector_buf_ndx);
			}


			// End of Field mark
			writeVal(0xde, rawsector_buf, rawsector_buf_ndx);
			writeVal(0xaa, rawsector_buf, rawsector_buf_ndx);
			writeVal(0xeb, rawsector_buf, rawsector_buf_ndx);

			// Some spacing sync bytes
			for (int i=0; i<14; i++) writeVal(0xff, rawsector_buf, rawsector_buf_ndx);

			// Some padding bytes
			for (int i=0; i<(512-416); i++) writeVal(0, rawsector_buf, rawsector_buf_ndx);

			if ( sizeof(rawsector_buf) != (iobytes = fwrite(rawsector_buf, sizeof(rawsector_buf[0]), ARRAYSIZE(rawsector_buf), fout)) )
			{
				fprintf(stderr, "deu merda!\n");
				return -2;
			}
		}
	}
}

// TODO: ler da linha de comando os par�metros: DISKVOLUME, SKEW FACTOR e numero de setores.
// por default, usa o padr�oz�o para DOS 3.3. Op��es para ProDOS, CP/M ou custom.
// O n�mero de trilhas � inferido pela quantidade de setores por trilha e o tamanho do arquivo.

int main(int argc, char * argv[])
{
	// TODO: Parse command line to feed the variables below if needed
	{
		unsigned char disk_volume = 0xfe;
		unsigned char const * skew = SKEW_PHYSICAL_TO_DOS;
		unsigned int num_sector_per_track = ARRAYSIZE(SKEW_PHYSICAL_TO_DOS);
		FILE * fin = freopen(NULL, "rb", stdin);
		FILE * fout = freopen(NULL, "rb", stdin);

		return conv_to_nic(
			disk_volume, skew, num_sector_per_track,
			fin, fout
		);
	}
}
