#!/usr/bin/env bash
gcc --std=c99 -D_FORTIFY_SOURCE=2 -g -O0 to_nic.c -o to_nic
gcc --std=c99 -D_FORTIFY_SOURCE=2 -g -O0 from_nic.c -o from_nic
