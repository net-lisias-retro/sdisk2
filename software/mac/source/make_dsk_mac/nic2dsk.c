#include <Carbon.h>

static EventHandlerRef appHandlerRef;

void conv(FSSpec *fsp_r);

static int RunningOnCarbonX(void)
{
	UInt32 response;
	
	return (Gestalt(gestaltSystemVersion, (SInt32 *)&response) == noErr)
		&& (response >= 0x01000);
}

void conv(FSSpec *fsp_r)
{
	OSErr aOSErr;
	SInt16 aRefNum_r, aRefNum_w;
	long len;
	unsigned char src[512],dst[258];
	unsigned int track, sector;
	FSSpec fsp_w;
	unsigned char *c;
	static unsigned char decTable[] = {
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x01,0x00,0x00,0x02,0x03,0x00,0x04,0x05,0x06,
		0x00,0x00,0x00,0x00,0x00,0x00,0x07,0x08,0x00,0x00,0x00,0x09,0x0a,0x0b,0x0c,0x0d,
		0x00,0x00,0x0e,0x0f,0x10,0x11,0x12,0x13,0x00,0x14,0x15,0x16,0x17,0x18,0x19,0x1a,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x1b,0x00,0x1c,0x1d,0x1e,
		0x00,0x00,0x00,0x1f,0x00,0x00,0x20,0x21,0x00,0x22,0x23,0x24,0x25,0x26,0x27,0x28,
		0x00,0x00,0x00,0x00,0x00,0x29,0x2a,0x2b,0x00,0x2c,0x2d,0x2e,0x2f,0x30,0x31,0x32,
		0x00,0x00,0x33,0x34,0x35,0x36,0x37,0x38,0x00,0x39,0x3a,0x3b,0x3c,0x3d,0x3e,0x3f
	};
	static unsigned int revScramble[] = {
	    0,13,11,9,7,5,3,1,14,12,10,8,6,4,2,15
	};	
	static unsigned char FlipBit[4] = { 0,  2,  1,  3  };

	c = (*fsp_r).name;
	len = c[0];

	if	(!(
		(c[len-3]=='.') &&	
		((c[len-2]=='N') || (c[len-2]=='n')) && 
		((c[len-1]=='I') || (c[len-1]=='i')) &&
		((c[len-0]=='C') || (c[len-0]=='c')))) return;

	fsp_w = *fsp_r;
	fsp_w.name[len-2] = 'D';
	fsp_w.name[len-1] = 'S';	
	fsp_w.name[len-0] = 'K';

	aOSErr = FSpOpenDF(fsp_r, fsCurPerm, &aRefNum_r);
	if (aOSErr == fnfErr) return;
	SetFPos(aRefNum_r, fsFromStart, 0L);

	aOSErr = FSpOpenDF(&fsp_w, fsCurPerm, &aRefNum_w);
	if (aOSErr != fnfErr) {
		if (Alert(128,nil)!=1) {
			FSClose(aRefNum_r);
			return;
		}	
	} else {
		aOSErr = FSpCreate(&fsp_w, '????', '????', smSysScript);
		aOSErr = FSpOpenDF(&fsp_w,  fsCurPerm, &aRefNum_w);
	}
	aOSErr = SetEOF(aRefNum_w, 0L);
	aOSErr = SetFPos(aRefNum_w, fsFromStart, 0L);

	for (track = 0; track < 35; track++) {
		for (sector = 0; sector < 16; sector++) {
			int i, j;
			unsigned char x, ox = 0;
			SetFPos(aRefNum_r, fsFromStart, track*(512*16)+revScramble[sector]*512);
			len=512L;
			FSRead(aRefNum_r, &len, src);
			for (j=0, i=0x38; i<0x8e; i++, j++) {
				x = ((ox^decTable[src[i]])&0x3f);
				dst[j+172] = FlipBit[(x>>4)&3];
				dst[j+86] = FlipBit[(x>>2)&3];
				dst[j] = FlipBit[(x)&3];
				ox = x;
			}
			for (j=0, i=0x8e; i<0x18e; i++, j++) {
				x = ((ox^decTable[src[i]])&0x3f);
				dst[j]|=(x<<2);
				ox = x;
			}
			len=256L;
			FSWrite(aRefNum_w, &len, dst);
		}
	}
	FSClose(aRefNum_r);
	FSClose(aRefNum_w);	
}

static pascal OSStatus DoAppCommandProcess(EventHandlerCallRef nextHandler, EventRef theEvent,
	void* userData)
{
	unsigned long ekind;
	long cls;
	HICommand aCommand;
	OSStatus result = eventNotHandledErr;

	cls = GetEventClass(theEvent);
	ekind = GetEventKind(theEvent);
	if (cls == kEventClassCommand) {
		GetEventParameter(theEvent, kEventParamDirectObject, typeHICommand,
			NULL, sizeof(HICommand), NULL, &aCommand);
		// menu
		switch (aCommand.commandID) {
		case kHICommandQuit:
			QuitApplicationEventLoop();
			result = noErr;
			break;
		}
	}
	return result;
}		

static pascal OSErr OpenEvent(const AppleEvent *event, AppleEvent *reply, long refcon)
{
	AEDescList dolist;
	long ct, len;
	AEKeyword key;
	DescType rtype;
	FSSpec fsc;
	OSStatus result = eventNotHandledErr;

	if (!AEGetParamDesc(event, keyDirectObject, typeAEList, &dolist)) {
		AECountItems(&dolist, &ct);
		if (ct >= 1) {
			if (!AEGetNthPtr(&dolist, 1, typeFSS, &key, &rtype, (Ptr)&fsc,
				(long)sizeof(FSSpec), &len)) {
				conv(&fsc);
				QuitApplicationEventLoop();
				return noErr;
			}
		}
	}
	return result;
}

static pascal OSErr QuitEvent(const AppleEvent *event, AppleEvent *reply, long refcon)
{
	QuitApplicationEventLoop();
	return noErr;
}

static void Initialize(void)
{
	InitCursor();
}

int main(void)
{
	OSErr err;

	Initialize();

	if (!RunningOnCarbonX()) {
		EventTypeSpec AppEventTypes[] = {{kEventClassCommand, kEventCommandProcess}};
		Handle menuBar;
		menuBar = GetNewMBar(128);
		SetMenuBar(menuBar);
		DrawMenuBar();
		SetMenuItemCommandID(GetMenuRef(128), 1, 'quit');		
		err = InstallApplicationEventHandler(NewEventHandlerUPP(DoAppCommandProcess),
			1, AppEventTypes, NULL, &appHandlerRef);
	}

	err = AEInstallEventHandler(kCoreEventClass, kAEOpenDocuments,
		NewAEEventHandlerUPP((AEEventHandlerProcPtr)OpenEvent), NULL, false);
	if (err != noErr) ExitToShell();

	err = AEInstallEventHandler(kCoreEventClass, kAEQuitApplication,
		NewAEEventHandlerUPP((AEEventHandlerProcPtr)QuitEvent), NULL, false);
	if (err != noErr) ExitToShell();

	RunApplicationEventLoop();

	return 0;
}