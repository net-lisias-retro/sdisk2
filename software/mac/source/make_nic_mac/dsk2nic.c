#include <Carbon.h>

static EventHandlerRef appHandlerRef;

static int RunningOnCarbonX(void)
{
	UInt32 response;
	
	return (Gestalt(gestaltSystemVersion, (SInt32 *)&response) == noErr)
		&& (response >= 0x01000);
}

void writeVal(unsigned char val, unsigned short fRef)
{
	long len = 1;
	unsigned char buf[1];
	
	buf[0] = val;
	FSWrite(fRef, &len, buf);
}

void writeAAVal(unsigned char val, unsigned short fRef)
{
	long len = 2;
	unsigned char buf[2];
	
	buf[0] = (0xaa | (val >> 1));
	buf[1] = (0xaa | val);
	FSWrite(fRef, &len, buf);
}

void conv(FSSpec *fsp_r)
{
	OSErr aOSErr;
	SInt16 aRefNum_r, aRefNum_w;
	long len;
	static unsigned char encTable[] = {
		0x96,0x97,0x9A,0x9B,0x9D,0x9E,0x9F,0xA6,
		0xA7,0xAB,0xAC,0xAD,0xAE,0xAF,0xB2,0xB3,
		0xB4,0xB5,0xB6,0xB7,0xB9,0xBA,0xBB,0xBC,
		0xBD,0xBE,0xBF,0xCB,0xCD,0xCE,0xCF,0xD3,
		0xD6,0xD7,0xD9,0xDA,0xDB,0xDC,0xDD,0xDE,
		0xDF,0xE5,0xE6,0xE7,0xE9,0xEA,0xEB,0xEC,
		0xED,0xEE,0xEF,0xF2,0xF3,0xF4,0xF5,0xF6,
		0xF7,0xF9,0xFA,0xFB,0xFC,0xFD,0xFE,0xFF
	};
	static unsigned char scramble[] = {
		0,7,14,6,13,5,12,4,11,3,10,2,9,1,8,15
	};
	static unsigned char FlipBit1[4] = { 0, 2,  1,  3  };
	static unsigned char FlipBit2[4] = { 0, 8,  4,  12 };
	static unsigned char FlipBit3[4] = { 0, 32, 16, 48 };
	unsigned char src[256+2], dst[384];
	int volume = 0xfe;
	int track, sector;
	unsigned char *c;
	FSSpec fsp_w;

	c = (*fsp_r).name;
	len = c[0];

	if	(!(
		(c[len-3]=='.') &&	
		((c[len-2]=='D') || (c[len-2]=='d')) && 
		((c[len-1]=='S') || (c[len-1]=='s')) &&
		((c[len-0]=='K') || (c[len-0]=='k')))) return;

	fsp_w = *fsp_r;
	fsp_w.name[len-2] = 'N';
	fsp_w.name[len-1] = 'I';	
	fsp_w.name[len-0] = 'C';
	
	aOSErr = FSpOpenDF(fsp_r, fsCurPerm, &aRefNum_r);
	if (aOSErr == fnfErr) return;
	SetFPos(aRefNum_r, fsFromStart, 0L);

	aOSErr = FSpOpenDF(&fsp_w, fsCurPerm, &aRefNum_w);
	if (aOSErr != fnfErr) {
		if (Alert(128,nil) != 1) {
			FSClose(aRefNum_r);
			return;	
		}	
	} else {
		aOSErr = FSpCreate(&fsp_w, '????', '????', smSysScript);
		aOSErr = FSpOpenDF(&fsp_w,  fsCurPerm, &aRefNum_w);
	}
	aOSErr = SetEOF(aRefNum_w, 0L);
	aOSErr = SetFPos(aRefNum_w, fsFromStart, 0L);

	for (track = 0; track < 35; track++) {
		for (sector = 0; sector < 16; sector++) {
			int i;
			unsigned char x, ox = 0;
			
			SetFPos(aRefNum_r, fsFromStart, track*(256*16)+scramble[sector]*256);
			for (i=0; i<22; i++) writeVal(0xff, aRefNum_w);
					
			writeVal(0x03, aRefNum_w);
			writeVal(0xfc, aRefNum_w);
			writeVal(0xff, aRefNum_w);
			writeVal(0x3f, aRefNum_w);
			writeVal(0xcf, aRefNum_w);
			writeVal(0xf3, aRefNum_w);
			writeVal(0xfc, aRefNum_w);
			writeVal(0xff, aRefNum_w);
			writeVal(0x3f, aRefNum_w);
			writeVal(0xcf, aRefNum_w);					
			writeVal(0xf3, aRefNum_w);
			writeVal(0xfc, aRefNum_w);	
					
			writeVal(0xd5, aRefNum_w);
			writeVal(0xaa, aRefNum_w);
			writeVal(0x96, aRefNum_w);
			writeAAVal(volume, aRefNum_w);
			writeAAVal(track, aRefNum_w);
			writeAAVal(sector, aRefNum_w);
			writeAAVal(volume ^ track ^ sector, aRefNum_w);
			writeVal(0xde, aRefNum_w);
			writeVal(0xaa, aRefNum_w);
			writeVal(0xeb, aRefNum_w);

			for (i=0; i<5; i++) writeVal(0xff, aRefNum_w);
			writeVal(0xd5, aRefNum_w);
			writeVal(0xaa, aRefNum_w);
			writeVal(0xad, aRefNum_w);

			len = 256;
			FSRead(aRefNum_r, &len, src);
			src[256] = src[257] = 0x0;
			
			for (i = 0; i < 86; i++) {
				x = (FlipBit1[src[i]&3] | FlipBit2[src[i+86]&3] | FlipBit3[src[i+172]&3]);
				dst[i]=encTable[(x^ox)&0x3f];
				ox = x;
			}
			for (i = 0; i < 256; i++) {
				x = (src[i] >> 2);
				dst[i+86] = encTable[(x^ox)&0x3f];
				ox = x;
			}
			len = 342;
			FSWrite(aRefNum_w, &len, dst);
			
			writeVal(encTable[ox&0x3f], aRefNum_w);
			writeVal(0xde, aRefNum_w);
			writeVal(0xaa, aRefNum_w);
			writeVal(0xeb, aRefNum_w);
			for (i=0; i<14; i++) writeVal(0xff, aRefNum_w);
			for (i=0; i<(512-416); i++) writeVal(0, aRefNum_w);
		}
				
	}
	FSClose(aRefNum_r);
	FSClose(aRefNum_w);			
}

static pascal OSStatus DoAppCommandProcess(EventHandlerCallRef nextHandler, EventRef theEvent,
	void* userData)
{
	unsigned long ekind;
	long cls;
	HICommand aCommand;
	OSStatus result = eventNotHandledErr;

	cls = GetEventClass(theEvent);
	ekind = GetEventKind(theEvent);
	if (cls == kEventClassCommand) {
		GetEventParameter(theEvent, kEventParamDirectObject, typeHICommand,
			NULL, sizeof(HICommand), NULL, &aCommand);
		// menu
		switch (aCommand.commandID) {
		case kHICommandQuit:
			QuitApplicationEventLoop();
			result = noErr;
			break;
		}
	}
	return result;
}		

static pascal OSErr OpenEvent(const AppleEvent *event, AppleEvent *reply, long refcon)
{
	AEDescList dolist;
	long ct, len;
	AEKeyword key;
	DescType rtype;
	FSSpec fsc;
	OSStatus result = eventNotHandledErr;

	if (!AEGetParamDesc(event, keyDirectObject, typeAEList, &dolist)) {
		AECountItems(&dolist, &ct);
		if (ct >= 1) {
			if (!AEGetNthPtr(&dolist, 1, typeFSS, &key, &rtype, (Ptr)&fsc,
				(long)sizeof(FSSpec), &len)) {
				conv(&fsc);
				QuitApplicationEventLoop();
				return noErr;
			}
		}
	}
	return result;
}

static pascal OSErr QuitEvent(const AppleEvent *event, AppleEvent *reply, long refcon)
{
	QuitApplicationEventLoop();
	return noErr;
}

static void Initialize(void)
{
	InitCursor();
}

int main(void)
{
	OSErr err;

	Initialize();

	if (!RunningOnCarbonX()) {
		EventTypeSpec AppEventTypes[] = {{kEventClassCommand, kEventCommandProcess}};
		Handle menuBar;
		menuBar = GetNewMBar(128);
		SetMenuBar(menuBar);
		DrawMenuBar();
		SetMenuItemCommandID(GetMenuRef(128), 1, 'quit');		
		err = InstallApplicationEventHandler(NewEventHandlerUPP(DoAppCommandProcess),
			1, AppEventTypes, NULL, &appHandlerRef);
	}

	err = AEInstallEventHandler(kCoreEventClass, kAEOpenDocuments,
		NewAEEventHandlerUPP((AEEventHandlerProcPtr)OpenEvent), NULL, false);
	if (err != noErr) ExitToShell();

	err = AEInstallEventHandler(kCoreEventClass, kAEQuitApplication,
		NewAEEventHandlerUPP((AEEventHandlerProcPtr)QuitEvent), NULL, false);
	if (err != noErr) ExitToShell();

	RunApplicationEventLoop();

	return 0;
}